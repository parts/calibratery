BEGIN {
	printf ";\n; BOTTOM=%f, HT=%f, DELTA=%f, N=%d\n;\n", BOTTOM, HT, DELTA, N;
	T = BOTTOM;
}
function set_temp(M) {
	printf("%s S%f\n",M,LASTT=T);
}

match($0,"Z([[:digit:].]+)",mm) {
	Z = mm[1];
	T = BOTTOM-int(mm[1]/HT)*DELTA;
	# printf("; Z=%f, T=%f, LASTT=%f, =%s",Z,T,LASTT,$0);
	if(T!=LASTT) {
		print;
		set_temp("M104");
		next;
	}
}

$1=="M109" { set_temp("M109"); next; }
$1=="M104" { set_temp("M104"); next; }

	{ print }
END	{ }
