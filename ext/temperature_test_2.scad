// By Glyn Cowles March 2014
use <write/Write.scad> //WriteScad By Harlan Martin harlan@sutlog.com
// label width, not too wide or you will have lots of tedious infill
lbl_wd=1.5;
// label height, this is the value to search for when changing the temp 
lbl_ht=15; 
//bottom label temp
Start_temp=215;
//amount to decrement temperature
dec_temp=5;
//Number of labels
iterations=5;

/* [Hidden] */
txt_ht=lbl_ht*.7; 
lbl_length=lbl_ht*4;
mfont="stencil_fix.dxf"; // from romankornfeld http://www.thingiverse.com/thing:100779/#files

$fn=50;
width=lbl_ht; 
size=[lbl_length,width,lbl_wd];
circle1=txt_ht*.25;
circle2=txt_ht*.15;
circle3=txt_ht*.07;

for (i=[0:iterations-1]) {
	translate([0,0,i*width]) rotate(a=[90,0,0]) label(str(Start_temp-i*dec_temp));
	}
translate([3,0,0])rotate(a=[0,270,0]) support(width*iterations,15);
translate([lbl_length-2,0,0])rotate(a=[0,270,0]) support(width*iterations,15);

//--------------------------------------------------------------------------------
module support(sh,sw) {
linear_extrude(1.5) square([sh,sw]);	
}
//--------------------------------------------------------------------------------
module label(text) {
	difference() {
		roundedRect(size,3);
		translate([txt_ht/3,(width-txt_ht)/2,0]) write(text,t=lbl_wd,h=txt_ht,font=mfont);
		translate([txt_ht*3,(width-txt_ht)/2,0]) linear_extrude(lbl_wd) square([txt_ht*1.3,txt_ht]);	
		translate([txt_ht*5,(width-txt_ht)/2+circle1/2,0]) linear_extrude(lbl_wd) circle(circle1);	
		translate([txt_ht*5,(width-txt_ht)/2+txt_ht-circle2/2,0]) linear_extrude(lbl_wd) circle(circle2);	
		translate([txt_ht*5,(width/2)+circle3,0]) linear_extrude(lbl_wd) circle(circle3);	
	}
}
//--------------------------------------------------------------------------------
module roundedRect(size, radius) {
x = size[0];
y = size[1];
z = size[2];

linear_extrude(height=z)
hull() {
translate([radius, radius, 0])
circle(r=radius);

translate([x - radius, radius, 0])
circle(r=radius);

translate([x - radius, y - radius, 0])
circle(r=radius);

translate([radius, y - radius, 0])
circle(r=radius);
}
}